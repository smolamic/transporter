use transporter_test::*;
use std::{
    fs,
    path::Path,
};

#[test]
fn create_addressbook() {
    let addressbook: Addressbook = vec![
        Contact {
            name: "Donald Duck".to_string(),
            phone: vec![
                PhoneNumber {
                    type_: PhoneNumberType::Home,
                    number: "028492924838".to_string(),
                },
                PhoneNumber {
                    type_: PhoneNumberType::Custom {
                        name: "Buschfunk".to_string(),
                    },
                    number: "02493023572035".to_string(),
                },
            ],
        },
        Contact {
            name: "Daisy Duck".to_string(),
            phone: vec![
                PhoneNumber {
                    type_: PhoneNumberType::Mobile,
                    number: "0238420415".to_string(),
                },
            ],
        },
    ];

    let path = Path::new("../reference/addressbook/ducks.json");
    match path.parent() {
        Some(dir) => fs::create_dir_all(dir).unwrap(),
        None => (),
    }
    let file = fs::File::create(path).unwrap();

    serde_json::to_writer(&file, &addressbook).unwrap();

    println!("{}", serde_json::to_string(&addressbook).unwrap());


}
