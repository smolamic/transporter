<?php namespace TransporterTest\Test;

require(__DIR__.'/../vendor/autoload.php');

use TransporterTest\Addressbook\Addressbook;
use TransporterTest\Addressbook\Contact;
use TransporterTest\Addressbook\PhoneNumber;
use TransporterTest\Addressbook\PhoneNumberType;
use \PHPUnit\Framework\TestCase;

class AddressbookTest extends TestCase {
    private $ducks;

    protected function setUp() : void
    {
        $this->ducks = file_get_contents(__DIR__.'/../../reference/addressbook/ducks.json');
    }

    public function testSetupAndEqualsReference() : void
    {
        $addressbook = new Addressbook([
            new Contact([
                'name' => "Donald Duck",
                'phone' => [
                    new PhoneNumber([
                        'type' => new PhoneNumberType\Home(),
                        'number' => "028492924838",
                    ]),
                    new PhoneNumber([
                        'type' => new PhoneNumberType\Custom([
                            'name' => "Buschfunk",
                        ]),
                        'number' => "02493023572035",
                    ]),
                ],
            ]),
            new Contact([
                'name' => "Daisy Duck",
                'phone' => [
                    new PhoneNumber([
                        'type' => new PhoneNumberType\Mobile(),
                        'number' => "0238420415",
                    ]),
                ],
            ]),
        ]);

        $this->assertEquals(
            json_decode($this->ducks),
            json_decode(json_encode($addressbook))
        );
    }

    public function testDeserializeReference() : void
    {
        $addressbook = Addressbook::deserialize(json_decode($this->ducks, true));

        $this->assertSame('Donald Duck', $addressbook->get()[0]->getName());
    }

    public function testDeserializeWrongString() : void
    {
        $this->expectErrorMessage('TransporterTest\Addressbook\PhoneNumberType::deserialize: Unknown Variant Fax');
        PhoneNumberType::deserialize('Fax');
    }

    public function testDeserializeWrongObject() : void
    {
        $this->expectErrorMessage('TransporterTest\Addressbook\PhoneNumberType::deserialize: Unknown Variant {"Fax":[]}');
        PhoneNumberType::deserialize(['Fax' => []]);
    }
}
