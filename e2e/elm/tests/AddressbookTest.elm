module AddressbookTest exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import Test exposing (..)
import Addressbook exposing (Addressbook, Contact, PhoneNumber, PhoneNumberType(..))


suite : Test
suite =
    describe "set up example addressbook"
        [ test "create PhoneNumberType" <|
            \_ ->
                let
                    home = Home
                    mobile = Mobile
                    workMobile = Custom { name = "Work Mobile" }
                in
                Expect.equal (home, mobile, workMobile) (Home, Mobile, Custom { name = "Work Mobile" })
        ]
