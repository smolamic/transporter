{{camel}}Version : String
{{camel}}Version = "{{name}}@{{version}}"

{{camel}}ContentType : String
{{camel}}ContentType = "application/json; ttype={{name}}@{{version}}"

type alias {{cap}} = {{elmtype}}
