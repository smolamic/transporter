{{camel}}Version : String
{{camel}}Version = "{{name}}@{{version}}"

{{camel}}ContentType : String
{{camel}}ContentType = "application/json; ttype={{name}}@{{version}}"

type {{cap}}
{%- for variant in variants %}
    {%- if loop.first %}
    {%- if variant.fields.is_empty() %}
    = {{variant.cap}}
    {%- else %}
    = {{variant.cap}}
        {%- for field in variant.fields %}
        {%- if loop.first %}
        { {{field.camel}} : {{field.elmtype}}
        {%- else %}
        , {{field.camel}} : {{field.elmtype}}
        {%- endif %}
        {%- endfor %}
        }
    {%- endif %}
    {%- else %}
    {%- if variant.fields.is_empty() %}
    | {{variant.cap}}
    {%- else %}
    | {{variant.cap}}
        {%- for field in variant.fields %}
        {%- if loop.first %}
        { {{field.camel}} : {{field.elmtype}}
        {%- else %}
        , {{field.camel}} : {{field.elmtype}}
        {%- endif %}
        {%- endfor %}
        }
    {%- endif %}
    {%- endif %}
{%- endfor %}
