{{camel}}Version : String
{{camel}}Version = "{{name}}@{{version}}"

{{camel}}ContentType : String
{{camel}}ContentType = "application/json; ttype={{name}}@{{version}}"
{% if fields.is_empty() -%}
type alias {{cap}} = {}
{%- else %}
type alias {{cap}} =
{%- for field in fields %}
    {%- if loop.first %}
    { {{field.camel}} : {{field.elmtype}}
    {%- else %}
    , {{field.camel}} : {{field.elmtype}}
    {%- endif %}
{%- endfor %}
    }
{%- endif %}
