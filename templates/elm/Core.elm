module {{module}} exposing (..)

import Dict exposing (Dict)
{% for type_info in types %}

{{type_info.core|safe}}
{% endfor %}
