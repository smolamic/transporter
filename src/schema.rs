use crate::error::{Error, Result};
use maplit::hashmap;
use serde::Deserialize;
use std::{
    collections::{hash_map::Values, HashMap},
    fs,
    path::Path,
    rc::Rc,
};
use toml::de;

type StructDef = HashMap<String, String>;
type VariantDef = HashMap<String, String>;
type UnionDef = HashMap<String, VariantDef>;

#[derive(Deserialize)]
struct AliasDef {
    #[serde(rename = "type")]
    type_: String,
}

#[derive(Deserialize)]
struct SchemaDef {
    transporter: String,
    version: String,
    #[serde(default = "HashMap::new")]
    alias: HashMap<String, AliasDef>,
    #[serde(rename = "struct", default = "HashMap::new")]
    struct_: HashMap<String, StructDef>,
    #[serde(default = "HashMap::new")]
    union: HashMap<String, UnionDef>,
}

impl SchemaDef {
    fn try_from_file(path: &Path) -> Result<Self> {
        Ok(de::from_slice(&fs::read(path)?)?)
    }
}

pub(crate) type Field = Rc<Type>;
pub(crate) type Variant = HashMap<String, Field>;

#[derive(Debug, PartialEq)]
pub(crate) enum Type {
    String,
    Int,
    Float,
    Bool,
    List(Rc<Type>),
    Dict(Rc<Type>),
    Optional(Rc<Type>),
    Alias(String, Rc<Type>),
    Struct(String, HashMap<String, Field>),
    Union(String, HashMap<String, Variant>),
}

impl Type {
    pub(crate) fn name(&self) -> String {
        match self {
            Self::String => "String".to_string(),
            Self::Int => "Int".to_string(),
            Self::Float => "Float".to_string(),
            Self::Bool => "Bool".to_string(),
            Self::List(inner) => format!("[{}]", inner.name()),
            Self::Dict(inner) => format!("{{{}}}", inner.name()),
            Self::Optional(inner) => format!("({})", inner.name()),
            Self::Alias(name, _) => name.clone(),
            Self::Struct(name, _) => name.clone(),
            Self::Union(name, _) => name.clone(),
        }
    }

    pub(crate) fn needs_dict(&self) -> bool {
        match self {
            Self::Dict(_) => true,
            Self::List(inner) => inner.needs_dict(),
            Self::Optional(inner) => inner.needs_dict(),
            _ => false,
        }
    }

    pub(crate) fn needs_optional(&self) -> bool {
        match self {
            Self::Optional(_) => true,
            Self::Dict(inner) => inner.needs_optional(),
            Self::List(inner) => inner.needs_optional(),
            _ => false,
        }
    }
}

type TypeMap = HashMap<String, Rc<Type>>;

fn insert_once(map: &mut TypeMap, key: String, type_: Type) -> Result<Rc<Type>> {
    let type_ = Rc::new(type_);

    if map.contains_key(&key) {
        Err(Error::DuplicateType(key.to_string()))
    } else {
        let _ = map.insert(key, type_.clone());
        Ok(type_)
    }
}

pub(crate) struct Schema {
    version: String,
    types: TypeMap,
}

fn resolve(name: &str, types: &mut TypeMap, schema: &mut SchemaDef) -> Result<Rc<Type>> {
    // if this type has already been generated return that
    if let Some(type_) = types.get(name) {
        return Ok(type_.clone());
    }

    if name.starts_with('(') {
        if !name.ends_with(')') {
            return Err(Error::UnmatchedDelimiter("(", name.to_string()));
        }

        let optional = Type::Optional(resolve(&name[1..(name.len() - 1)], types, schema)?);
        return insert_once(types, name.to_string(), optional);
    }

    if name.starts_with('[') {
        if !name.ends_with(']') {
            return Err(Error::UnmatchedDelimiter("[", name.to_string()));
        }

        let list = Type::List(resolve(&name[1..(name.len() - 1)], types, schema)?);
        return insert_once(types, name.to_string(), list);
    }

    if name.starts_with('{') {
        if !name.ends_with('}') {
            return Err(Error::UnmatchedDelimiter("{", name.to_string()));
        }
        let dict = Type::Dict(resolve(&name[1..(name.len() - 1)], types, schema)?);
        return insert_once(types, name.to_string(), dict);
    }

    if let Some((name, def)) = schema.alias.remove_entry(name) {
        let alias = create_alias(name.clone(), def, types, schema)?;
        return insert_once(types, name, alias);
    }

    if let Some((name, def)) = schema.struct_.remove_entry(name) {
        let struct_ = create_struct(name.clone(), def, types, schema)?;
        return insert_once(types, name, struct_);
    }

    if let Some((name, def)) = schema.union.remove_entry(name) {
        let union = create_union(name.clone(), def, types, schema)?;
        return insert_once(types, name, union);
    }

    Err(Error::UnresolvedType(name.to_string()))
}

fn create_alias(
    name: String,
    def: AliasDef,
    types: &mut TypeMap,
    schema: &mut SchemaDef,
) -> Result<Type> {
    Ok(Type::Alias(name, resolve(&def.type_, types, schema)?))
}

fn create_struct(
    name: String,
    def: StructDef,
    types: &mut TypeMap,
    schema: &mut SchemaDef,
) -> Result<Type> {
    let len = def.len();
    Ok(Type::Struct(
        name,
        def.into_iter().fold(
            Ok(HashMap::with_capacity(len)),
            |fields: Result<HashMap<String, Field>>, (name, type_)| {
                let mut fields = fields?;
                let _ = fields.insert(name, resolve(&type_, types, schema)?);
                Ok(fields)
            },
        )?,
    ))
}

fn create_union(
    name: String,
    def: UnionDef,
    types: &mut TypeMap,
    schema: &mut SchemaDef,
) -> Result<Type> {
    let len = def.len();
    if len == 0 {
        return Err(Error::EmptyUnion(name));
    }
    Ok(Type::Union(
        name,
        def.into_iter().fold(
            Ok(HashMap::with_capacity(len)),
            |variants: Result<HashMap<String, Variant>>, (name, fields)| {
                let mut variants = variants?;
                let len = fields.len();
                let _ = variants.insert(
                    name,
                    fields.into_iter().fold(
                        Ok(HashMap::with_capacity(len)),
                        |fields: Result<HashMap<String, Field>>, (name, type_)| {
                            let mut fields = fields?;
                            fields.insert(name, resolve(&type_, types, schema)?);
                            Ok(fields)
                        },
                    )?,
                );
                Ok(variants)
            },
        )?,
    ))
}

const VERSION: &'static str = env!("CARGO_PKG_VERSION");

impl Schema {
    fn try_from(mut def: SchemaDef) -> Result<Self> {
        let mut types = hashmap! {
            "string".to_string() => Rc::new(Type::String),
            "int".to_string() => Rc::new(Type::Int),
            "float".to_string() => Rc::new(Type::Float),
            "bool".to_string() => Rc::new(Type::Bool),
        };

        let names: Vec<String> = def
            .alias
            .keys()
            .chain(def.struct_.keys())
            .chain(def.union.keys())
            .map(|name| name.clone())
            .collect();

        for name in names {
            let _ = resolve(&name, &mut types, &mut def)?;
        }

        let version_req = semver::VersionReq::parse(&format!("^{}", def.transporter))?;
        let bin_version = semver::Version::parse(VERSION)?;
        if !version_req.matches(&bin_version) {
            return Err(Error::IncompatibleVersion(def.transporter));
        }

        Ok(Self {
            version: def.version,
            types,
        })
    }

    pub(crate) fn try_from_file(path: &Path) -> Result<Self> {
        Self::try_from(SchemaDef::try_from_file(path)?)
    }

    pub(crate) fn types(&self) -> Values<String, Rc<Type>> {
        self.types.values()
    }

    pub(crate) fn version(&self) -> &str {
        &self.version
    }
}

#[cfg(test)]
mod tests {
    use super::{Schema, SchemaDef, Type};
    use std::{path::Path, rc::Rc};

    #[test]
    fn build_addressbook_schema_def() {
        let schema = SchemaDef::try_from_file(Path::new("schema/Addressbook.toml")).unwrap();

        assert_eq!("0.1.0", schema.version);
        assert_eq!("[Contact]", schema.alias.get("Addressbook").unwrap().type_);
        assert_eq!(
            "string",
            schema.struct_.get("Contact").unwrap().get("name").unwrap()
        );
        assert_eq!(
            "[PhoneNumber]",
            schema.struct_.get("Contact").unwrap().get("phone").unwrap()
        );
        assert_eq!(
            "PhoneNumberType",
            schema
                .struct_
                .get("PhoneNumber")
                .unwrap()
                .get("type")
                .unwrap()
        );
        assert_eq!(
            "string",
            schema
                .struct_
                .get("PhoneNumber")
                .unwrap()
                .get("number")
                .unwrap()
        );
        assert!(schema
            .union
            .get("PhoneNumberType")
            .unwrap()
            .get("Mobile")
            .unwrap()
            .is_empty());
        assert!(schema
            .union
            .get("PhoneNumberType")
            .unwrap()
            .get("Home")
            .unwrap()
            .is_empty());
        assert!(schema
            .union
            .get("PhoneNumberType")
            .unwrap()
            .get("Work")
            .unwrap()
            .is_empty());
        assert_eq!(
            "string",
            schema
                .union
                .get("PhoneNumberType")
                .unwrap()
                .get("Custom")
                .unwrap()
                .get("name")
                .unwrap()
        );
    }

    #[test]
    fn build_addressbook_schema() {
        let schema = Schema::try_from_file(Path::new("schema/Addressbook.toml")).unwrap();

        assert_eq!("0.1.0", schema.version);

        match schema.types.get("PhoneNumberType").unwrap().as_ref() {
            Type::Union(_, variants) => {
                assert!(variants.get("Mobile").unwrap().is_empty());
                assert_eq!(
                    &Rc::new(Type::String),
                    variants.get("Custom").unwrap().get("name").unwrap(),
                    "PhoneNumberType::Custom::name should be a string"
                );
            }
            _ => panic!("PhoneNumberType should be a Union"),
        }
    }
}
