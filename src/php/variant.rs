use crate::{error::Result, php::field::Field, schema, util::Indent, CodeGenerator, FileCreator};
use inflector::Inflector;
use std::{io::Write, path::PathBuf};

pub(crate) struct Variant<'n, 'ns, 'p, 'f> {
    name: &'n str,
    cap: String,
    namespace: &'ns str,
    parent: &'p str,
    path: PathBuf,
    fields: Vec<Field<'f>>,
}

impl<'n, 'ns, 'p, 'f> CodeGenerator for Variant<'n, 'ns, 'p, 'f> {
    fn build<C>(&self, fc: &mut C) -> Result<()>
    where
        C: FileCreator,
    {
        let content = if self.fields.is_empty() {
            format!(
                "<?php namespace {namespace}\\{parent};

class {cap} extends \\{namespace}\\{parent} implements \\JsonSerializable {{
    /**
     * @return mixed
     */
    public function jsonSerialize()
    {{
        return '{name}';
    }}
}}
",
                namespace = self.namespace,
                name = self.name,
                cap = self.cap,
                parent = self.parent,
            )
        } else {
            let vars: Vec<String> = self.fields.iter().map(Field::var).collect();
            let accessors: Vec<String> = self.fields.iter().map(Field::accessors).collect();
            let initializers: Vec<String> = self.fields.iter().map(Field::initializer).collect();
            let serializers: Vec<String> = self.fields.iter().map(Field::serializer).collect();

            format!(
                "<?php namespace {namespace}\\{parent};

class {cap} extends \\{namespace}\\{parent} implements \\JsonSerializable {{
{vars}

    /**
     * @param mixed[] $fields
     */
    public function __construct(array $fields)
    {{{initializers}
    }}
{accessors}

    /**
     * @return mixed
     */
    public function jsonSerialize()
    {{
        return ['{name}' => [{serializers}
        ]];
    }}
}}
",
                namespace = self.namespace,
                name = self.name,
                cap = self.cap,
                parent = self.parent,
                initializers = initializers.join("").indent(8),
                vars = vars.join("").indent(4),
                accessors = accessors.join("").indent(4),
                serializers = serializers.join("").indent(12),
            )
        };

        fc.create(&self.path)?.write_all(&content.into_bytes())?;

        Ok(())
    }
}

impl<'n, 'ns, 'p, 'f> Variant<'n, 'ns, 'p, 'f> {
    pub(crate) fn case(&self) -> (bool, String) {
        if self.fields.is_empty() {
            (
                true,
                format!(
                    "
case '{name}':
    return new \\{namespace}\\{parent}\\{cap}();",
                    name = self.name,
                    cap = self.cap,
                    namespace = self.namespace,
                    parent = self.parent,
                ),
            )
        } else {
            let deserializers: Vec<String> = self.fields.iter().map(Field::deserializer).collect();

            (
                false,
                format!(
                    "
if (array_key_exists('{name}', $input)) {{
    $input = $input['{name}'];
{deserializers}
    return new \\{namespace}\\{parent}\\{cap}($input);
}}",
                    name = self.name,
                    cap = self.cap,
                    deserializers = deserializers.join("").indent(4),
                    namespace = self.namespace,
                    parent = self.parent,
                ),
            )
        }
    }

    pub(crate) fn from(
        dir: &PathBuf,
        namespace: &'ns str,
        parent: &'p str,
        name: &'n str,
        fields: &'f schema::Variant,
    ) -> Self {
        let mut variant_dir = dir.clone();
        variant_dir.push(name);

        let mut path = dir.clone();
        path.push(parent);
        path.push(format!("{}.php", name));

        let fields: Vec<Field<'f>> = fields
            .iter()
            .map(|(name, type_)| Field::build(namespace, name, type_))
            .collect();

        Self {
            namespace,
            name,
            parent,
            cap: name.to_pascal_case(),
            fields,
            path,
        }
    }
}
