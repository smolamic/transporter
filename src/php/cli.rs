use crate::{
    code_generator::CodeGenerator,
    error::Result,
    php::{alias::Alias, struct_::Struct, union::Union},
    schema::{Schema, Type},
    FileCreator,
};

use std::{io::Write, path::PathBuf};

use structopt::StructOpt;
use typed_builder::TypedBuilder;

#[derive(StructOpt, TypedBuilder)]
pub struct Cli {
    #[structopt(parse(from_os_str))]
    #[builder(setter(into))]
    output: PathBuf,
    #[structopt(short = "n", long = "namespace", default_value = "Transport")]
    #[builder(default = "Transport".to_string(), setter(into))]
    namespace: String,
    #[structopt(
        short = "c",
        long = "config",
        parse(from_os_str),
        default_value = "Schema.toml"
    )]
    #[builder(default = "Schema.toml".into(), setter(into))]
    config: PathBuf,
}

fn transport_type(namespace: &str, version: &str) -> String {
    format!(
        "<?php namespace {namespace};
interface TransportType {{
    const SCHEMA_VERSION = \"{version}\";

    /**
     * @param mixed $input
     * @return Self
     */
    public static function deserialize($input) : Self;
}}
",
        namespace = namespace,
        version = version,
    )
}

impl CodeGenerator for Cli {
    fn build<C>(&self, fc: &mut C) -> Result<()>
    where
        C: FileCreator,
    {
        let schema = Schema::try_from_file(&self.config)?;

        let mut transport_type_path = self.output.clone();
        transport_type_path.push("TransportType.php");

        fc.create(&transport_type_path)?
            .write_all(&transport_type(&self.namespace, schema.version()).into_bytes())?;

        for type_ in schema.types() {
            match type_.as_ref() {
                Type::Alias(name, inner) => Alias {
                    version: schema.version(),
                    dir: &self.output,
                    namespace: &self.namespace,
                    name,
                    type_: inner,
                }
                .build(fc)?,

                Type::Struct(name, fields) => Struct {
                    version: schema.version(),
                    dir: &self.output,
                    namespace: &self.namespace,
                    name,
                    fields,
                }
                .build(fc)?,

                Type::Union(name, variants) => Union {
                    version: schema.version(),
                    dir: &self.output,
                    namespace: &self.namespace,
                    name,
                    variants,
                }
                .build(fc)?,

                _ => (),
            }
        }

        Ok(())
    }
}
