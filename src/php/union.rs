use crate::{
    error::Result, php::variant::Variant, schema, util::Indent, CodeGenerator, FileCreator,
};
use inflector::Inflector;
use std::{collections::HashMap, io::Write, path::PathBuf};

pub(crate) struct Union<'v, 'd, 'ns, 'n, 'var> {
    pub(crate) version: &'v str,
    pub(crate) dir: &'d PathBuf,
    pub(crate) namespace: &'ns str,
    pub(crate) name: &'n str,
    pub(crate) variants: &'var HashMap<String, schema::Variant>,
}

impl<'v, 'd, 'ns, 'n, 'var> CodeGenerator for Union<'v, 'd, 'ns, 'n, 'var> {
    fn build<C>(&self, fc: &mut C) -> Result<()>
    where
        C: FileCreator,
    {
        let cap = self.name.to_pascal_case();

        let variants: Vec<Variant> = self
            .variants
            .iter()
            .map(|(variant_name, fields)| {
                Variant::from(&self.dir, &self.namespace, &cap, variant_name, fields)
            })
            .collect();

        let (plain_cases, structural_cases): (Vec<(bool, String)>, Vec<(bool, String)>) = variants
            .iter()
            .map(|variant| variant.case())
            .partition(|(plain, _)| *plain);

        let plain_cases = plain_cases
            .into_iter()
            .map(|(_, case)| case)
            .collect::<Vec<String>>();
        let structural_cases = structural_cases
            .into_iter()
            .map(|(_, case)| case)
            .collect::<Vec<String>>();

        for variant in variants {
            variant.build(fc)?;
        }

        let mut path = self.dir.clone();
        path.push(format!("{}.php", self.name));
        let mut file = fc.create(&path)?;

        let content = format!(
            "<?php namespace {namespace};

abstract class {cap} implements TransportType {{
    const TYPE_VERSION = \"{name}@{version}\";
    const CONTENT_TYPE = \"application/json; ttype={name}@{version}\";

    /**
     * @param mixed $input
     * @return TransportType
     */
    public static function deserialize($input) : TransportType
    {{
        if (is_array($input)) {{{structural_cases}
            $variant = json_encode($input);
            throw new \\Exception(\"{namespace}\\{name}::deserialize: Unknown Variant $variant\");
        }}

        switch ($input) {{{plain_cases}
        }}
        throw new \\Exception(\"{namespace}\\{name}::deserialize: Unknown Variant $input\");
    }}
}}
",
            namespace = self.namespace,
            name = self.name,
            cap = cap,
            plain_cases = plain_cases.join("").indent(8),
            structural_cases = structural_cases.join("").indent(12),
            version = self.version,
        );

        file.write_all(&content.into_bytes())?;

        Ok(())
    }
}
