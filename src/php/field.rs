use crate::{
    schema::Type,
    php::{
        php_type::PhpType,
    },
};
use inflector::Inflector;

pub(crate) struct Field<'n> {
    name: &'n str,
    member: String,
    cap: String,
    constructor: String,
    phptype: String,
    doctype: String,
}

impl<'n> Field<'n> {
    pub(crate) fn var(&self) -> String {
        format!("
/* @var {doctype} */
private ${member};",
            doctype = self.doctype,
            member = self.member,
        )
    }

    pub(crate) fn accessors(&self) -> String {
        format!("
/**
 * @param {doctype} $value
 */
public function set{cap}({phptype} $value)
{{
    $this->{member} = $value;
}}

/**
 * @return {doctype}
 */
public function get{cap}() : {phptype}
{{
    return $this->{member};
}}",
            phptype = self.phptype,
            doctype = self.doctype,
            member = self.member,
            cap = self.cap,
        )
    }

    pub(crate) fn initializer(&self) -> String {
        format!("
$this->set{cap}($fields['{name}']);",
            cap = self.cap,
            name = self.name,
        )
    }

    pub(crate) fn deserializer(&self) -> String {
        format!("
$input['{name}'] = {constructor};",
            name = self.name,
            constructor = self.constructor,
        )
    }

    pub(crate) fn serializer(&self) -> String {
        format!("
'{name}' => $this->{member},",
            name = self.name,
            member = self.member,
        )
    }

    pub(crate) fn build(namespace: &str, name: &'n str, type_: &Type) -> Self {

        Self {
            constructor: type_.constructor(namespace, &format!("$input['{}']", name)),
            doctype: type_.to_doctype(namespace),
            phptype: type_.to_php(namespace),
            cap: name.to_pascal_case(),
            member: name.to_camel_case(),
            name: name,
        }
    }
}
