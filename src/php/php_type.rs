use crate::{
    schema::Type,
};

pub(crate) enum Kind<'a> {
    Primitive,
    Array(&'a Type),
    Optional(&'a Type),
    Class(&'a str),
}

pub(crate) trait PhpType {
    fn to_php(&self, namespace: &str) -> String;
    fn to_doctype(&self, namespace: &str) -> String;
    fn kind(&self) -> Kind;
    fn constructor(&self, namespace: &str, arg: &str) -> String;
}

fn with_namespace(namespace: &str, type_: &str) -> String {
    format!("\\{}\\{}", namespace, type_)
}

impl PhpType for Type {
    fn to_php(&self, namespace: &str) -> String {
        match self {
            Type::String => "string".to_string(),
            Type::Int => "int".to_string(),
            Type::Float => "float".to_string(),
            Type::Bool => "bool".to_string(),
            Type::List(_) => "array".to_string(),
            Type::Dict(_) => "array".to_string(),
            Type::Optional(inner) => inner.to_php(namespace),
            Type::Alias(name, _) => with_namespace(namespace, name),
            Type::Struct(name, _) => with_namespace(namespace, name),
            Type::Union(name, _) => with_namespace(namespace, name),
        }
    }

    fn to_doctype(&self, namespace: &str) -> String {
        match self {
            Type::String => "string".to_string(),
            Type::Int => "int".to_string(),
            Type::Float => "float".to_string(),
            Type::Bool => "bool".to_string(),
            Type::List(inner) => format!("{}[]", inner.to_doctype(namespace)),
            Type::Dict(inner) => format!("{}[]", inner.to_doctype(namespace)),
            Type::Optional(inner) => format!("{}|null", inner.to_doctype(namespace)),
            Type::Alias(name, _) => with_namespace(namespace, name),
            Type::Struct(name, _) => with_namespace(namespace, name),
            Type::Union(name, _) => with_namespace(namespace, name),
        }
    }

    fn kind(&self) -> Kind {
        match self {
            Type::String => Kind::Primitive,
            Type::Int => Kind::Primitive,
            Type::Float => Kind::Primitive,
            Type::Bool => Kind::Primitive,
            Type::List(inner) => Kind::Array(inner),
            Type::Dict(inner) => Kind::Array(inner),
            Type::Optional(inner) => Kind::Optional(inner),
            Type::Alias(name, _) => Kind::Class(name),
            Type::Struct(name, _) => Kind::Class(name),
            Type::Union(name, _) => Kind::Class(name),
        }
    }

    fn constructor(
        &self,
        namespace: &str,
        arg: &str,
    ) -> String {
        let phptype = self.to_php(namespace);

        match self.kind() {
            Kind::Primitive => format!("({}) {}", phptype, arg),
            Kind::Array(inner) => format!(
                "array_map(function ($value) {{ return {}; }}, {})",
                inner.constructor(namespace, "$value"),
                arg,
            ),
            Kind::Optional(inner) => inner.constructor(namespace, arg),
            Kind::Class(name) => format!("new {}({})", name, arg),
        }
    }

}
