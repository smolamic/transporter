use crate::{error::Result, php::php_type::PhpType, schema::Type, CodeGenerator, FileCreator};
use std::{io::Write, path::PathBuf};

pub(crate) struct Alias<'v, 'd, 'ns, 'n, 't> {
    pub(crate) version: &'v str,
    pub(crate) dir: &'d PathBuf,
    pub(crate) namespace: &'ns str,
    pub(crate) name: &'n str,
    pub(crate) type_: &'t Type,
}

impl<'v, 'd, 'ns, 'n, 't> CodeGenerator for Alias<'v, 'd, 'ns, 'n, 't> {
    fn build<C>(&self, fc: &mut C) -> Result<()>
    where
        C: FileCreator,
    {
        let mut path = self.dir.clone();
        path.push(format!("{}.php", self.name));
        let mut file = fc.create(&path)?;

        let content = format!(
            "<?php namespace {namespace};

class {name} implements \\JsonSerializable, TransportType {{
    const TYPE_VERSION = \"{name}@{version}\";
    const CONTENT_TYPE = \"application/json; ttype={name}@{version}\";

    /* @var {doctype} */
    private $value;

    /**
     * @param {doctype} $value
     */
    public function __construct({type_} $value)
    {{
        $this->value = $value;
    }}

    /**
     * @param {doctype} $value
     */
    public function set({type_} $value)
    {{
        $this->value = $value;
    }}

    /**
     * @return {doctype}
     */
    public function get() : {type_}
    {{
        return $this->value;
    }}

    /**
     * @return mixed
     */
    public function jsonSerialize()
    {{
        return $this->value;
    }}

    /**
     * @param mixed $input
     * @return TransportType
     */
    public static function deserialize($input) : TransportType
    {{
        return new Self({constructor});
    }}
}}
",
            namespace = self.namespace,
            name = self.name,
            version = self.version,
            type_ = self.type_.to_php(self.namespace),
            doctype = self.type_.to_doctype(self.namespace),
            constructor = self.type_.constructor(self.namespace, "$input"),
        );

        file.write_all(&content.into_bytes())?;

        Ok(())
    }
}
