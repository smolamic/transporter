use crate::{
    code_generator::CodeGenerator, error::Result, php::field::Field, schema, util::Indent,
    FileCreator,
};
use std::{collections::HashMap, io::Write, path::PathBuf};

pub(crate) struct Struct<'v, 'p, 'ns, 'n, 'f> {
    pub(crate) version: &'v str,
    pub(crate) dir: &'p PathBuf,
    pub(crate) namespace: &'ns str,
    pub(crate) name: &'n str,
    pub(crate) fields: &'f HashMap<String, schema::Field>,
}

impl<'v, 'p, 'ns, 'n, 'f> CodeGenerator for Struct<'v, 'p, 'ns, 'n, 'f> {
    fn build<C>(&self, fc: &mut C) -> Result<()>
    where
        C: FileCreator,
    {
        let fields: Vec<Field> = self
            .fields
            .iter()
            .map(|(name, type_)| Field::build(self.namespace, name, type_))
            .collect();

        let vars: Vec<String> = fields.iter().map(Field::var).collect();
        let accessors: Vec<String> = fields.iter().map(Field::accessors).collect();
        let initializers: Vec<String> = fields.iter().map(Field::initializer).collect();
        let deserializers: Vec<String> = fields.iter().map(Field::deserializer).collect();
        let serializers: Vec<String> = fields.iter().map(Field::serializer).collect();

        let mut path = self.dir.clone();
        path.push(format!("{}.php", self.name));
        let mut file = fc.create(&path)?;

        let content = format!(
            "<?php namespace {namespace};

class {name} implements TransportType, \\JsonSerializable {{
    const TYPE_VERSION = \"{name}@{version}\";
    const CONTENT_TYPE = \"application/json; ttype={name}@{version}\";
{vars}

    /**
     * @param mixed[] $fields
     */
    public function __construct(array $fields)
    {{{initializers}
    }}
{accessors}

    /**
     * @param mixed $input
     * @return TransportType
     */
    public static function deserialize($input) : TransportType
    {{{deserializers}
        return new Self($input);
    }}

    /**
     * @return mixed
     */
    public function jsonSerialize()
    {{
        return [{serializers}
        ];
    }}
}}
",
            namespace = self.namespace,
            name = self.name,
            version = self.version,
            vars = vars.join("").indent(4),
            initializers = initializers.join("").indent(8),
            accessors = accessors.join("").indent(4),
            deserializers = deserializers.join("").indent(8),
            serializers = serializers.join("").indent(12),
        );

        file.write_all(&content.into_bytes())?;

        Ok(())
    }
}
