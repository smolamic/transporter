//! The crate's main binary

use transporter::{
    Cli,
    Result,
    CodeGenerator,
    FsFileCreator,
};
use structopt::StructOpt;


/**
read options passed to the CLI, set up FileCreator and build output files
writing to the file creator
*/
fn main() -> Result<()> {
    let cli = Cli::from_args();
    let mut fc = FsFileCreator::new();
    cli.build(&mut fc)?;
    Ok(())
}
