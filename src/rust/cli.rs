use crate::{
    code_generator::CodeGenerator,
    error::Result,
    rust::{alias, struct_, union},
    schema::{Schema, Type},
    FileCreator,
};

use std::{io::Write, path::PathBuf};

use structopt::StructOpt;
use typed_builder::TypedBuilder;

#[derive(StructOpt, TypedBuilder)]
pub struct Cli {
    #[structopt(parse(from_os_str))]
    #[builder(default = ".".into(), setter(into))]
    output: PathBuf,
    #[structopt(long = "visibility", default_value = "pub(crate)")]
    #[builder(default = "pub(crate)".to_string(), setter(into))]
    visibility: String,
    #[structopt(
        short = "c",
        long = "config",
        parse(from_os_str),
        default_value = "Schema.toml"
    )]
    #[builder(default = "Schema.toml".into(), setter(into))]
    config: PathBuf,
}

impl CodeGenerator for Cli {
    fn build<C>(&self, fc: &mut C) -> Result<()>
    where
        C: FileCreator,
    {
        let schema = Schema::try_from_file(&self.config)?;
        let mut types = Vec::new();

        for type_ in schema.types() {
            match type_.as_ref() {
                Type::Alias(name, inner) => types.push(alias::build(
                    schema.version(),
                    &self.visibility,
                    name,
                    inner,
                )),
                Type::Struct(name, fields) => types.push(struct_::build(
                    schema.version(),
                    &self.visibility,
                    name,
                    fields,
                )),
                Type::Union(name, variants) => types.push(union::build(
                    schema.version(),
                    &self.visibility,
                    name,
                    variants,
                )),
                _ => (),
            }
        }

        let content = format!(
            "
#[allow(dead_code)]
const SCHEMA_VERSION: &str = \"{version}\";
{types}",
            version = schema.version(),
            types = types.join(""),
        );

        let mut file = fc.create(&self.output)?;
        file.write_all(&content.into_bytes())?;
        Ok(())
    }
}
