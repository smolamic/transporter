use crate::{
    schema::{
        Type,
    },
};

pub(crate) fn to_rust(type_: &Type) -> String {
    match type_ {
        Type::String => "String".to_string(),
        Type::Int => "i64".to_string(),
        Type::Float => "f64".to_string(),
        Type::Bool => "bool".to_string(),
        Type::List(inner) => format!("Vec<{}>", to_rust(inner)),
        Type::Dict(inner) => format!("HashMap<String, {}>", to_rust(inner)),
        Type::Optional(inner) => format!("Option<{}>", to_rust(inner)),
        Type::Alias(name, _) => name.clone(),
        Type::Struct(name, _) => name.clone(),
        Type::Union(name, _) => name.clone(),
    }
}
