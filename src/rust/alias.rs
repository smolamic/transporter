use crate::{
    schema::Type,
    rust::type_,
};
use inflector::Inflector;

pub(crate) fn build(
    version: &str,
    visibility: &str,
    name: &str,
    inner: &Type,
) -> String {
    let upper: String = name.to_screaming_snake_case();
    let cap: String = name.to_pascal_case();

    format!("
#[allow(dead_code)]
{vis} type {cap} = {type_};

#[allow(dead_code)]
const {upper}_VERSION: &str = \"{name}@{version}\";
#[allow(dead_code)]
const {upper}_CONTENT_TYPE: &str = \"application/json; ttype={name}@{version}\";
",
        name = name,
        type_ = type_::to_rust(inner),
        vis = visibility,
        upper = upper,
        cap = cap,
        version = version,
    )
}
