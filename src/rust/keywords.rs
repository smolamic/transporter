const KEYWORDS: &[&str] = &[
    "Self",
    "abstract",
    "as",
    "async",
    "await",
    "become",
    "box",
    "break",
    "const",
    "continue",
    "crate",
    "do",
    "dyn",
    "else",
    "enum",
    "extern",
    "false",
    "final",
    "fn",
    "for",
    "if",
    "impl",
    "in",
    "let",
    "loop",
    "macro",
    "match",
    "mod",
    "move",
    "mut",
    "override",
    "priv",
    "pub",
    "ref",
    "return",
    "self",
    "static",
    "struct",
    "super",
    "trait",
    "true",
    "try",
    "type",
    "typeof",
    "unsafe",
    "unsized",
    "use",
    "virtual",
    "where",
    "while",
    "yield",
];

pub(crate) trait MaybeKeyword {
    fn is_keyword(&self) -> bool;
}

impl MaybeKeyword for str {
    fn is_keyword(&self) -> bool {
        KEYWORDS.binary_search(&self).is_ok()
    }
}

impl MaybeKeyword for String {
    fn is_keyword(&self) -> bool {
        KEYWORDS.binary_search(&(&self[..])).is_ok()
    }
}


#[cfg(test)]
mod tests {
    use super::{
        KEYWORDS,
        MaybeKeyword,
    };

    #[test]
    fn test_binary_search() {
        assert!(KEYWORDS.binary_search(&"Self").is_ok());
        assert!(KEYWORDS.binary_search(&"yield").is_ok());
    }

    #[test]
    fn test_is_keyword() {
        assert!("type".is_keyword());
        assert!(!"number".is_keyword());

        for keyword in KEYWORDS {
            assert!(keyword.is_keyword());
        }
    }
}
