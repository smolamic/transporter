use std::collections::HashMap;
use crate::{
    schema::Variant,
    rust::{
        type_,
        keywords::MaybeKeyword,
    },
};
use inflector::Inflector;

pub(crate) fn build(
    version: &str,
    visibility: &str,
    name: &str,
    variants: &HashMap<String, Variant>,
) -> String {
    let upper: String = name.to_screaming_snake_case();

    let variants: Vec<String> = variants.iter().map(|(name, fields)| {
        let fields: Vec<String> = fields.iter().map(|(name, type_)| {
            if name.is_keyword() {
                format!("
        #[serde(rename = \"{name}\")]
        {name}_: {type_},",
                    name = name,
                    type_ = type_::to_rust(type_),
                )
            } else {
                format!("
        {name}: {type_},",
                    name = name,
                    type_ = type_::to_rust(type_),
                )
            }
        }).collect();

        if fields.is_empty() {
            format!("
    {name},",
                name = name,
            )
        } else {

            format!("
    {name} {{{fields}
    }},",
                name = name,
                fields = fields.join(""),
            )
        }
    }).collect();

    format!("
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(dead_code)]
{vis} enum {name} {{{variants}
}}

#[allow(dead_code)]
const {upper}_VERSION: &str = \"{name}@{version}\";
#[allow(dead_code)]
const {upper}_CONTENT_TYPE: &str = \"application/json; ttype={name}@{version}\";
",
        name = name,
        vis = visibility,
        variants = variants.join(""),
        upper = upper,
        version = version,
    )
}
