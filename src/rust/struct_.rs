use std::collections::HashMap;
use crate::{
    schema::Field,
    rust::{
        type_,
        keywords::MaybeKeyword,
    },
};
use inflector::Inflector;

pub(crate) fn build(
    version: &str,
    visibility: &str,
    name: &str,
    fields: &HashMap<String, Field>
) -> String {
    let upper: String = name.to_screaming_snake_case();

    let fields: Vec<String> = fields.iter().map(|(name, type_)| {
        if name.is_keyword() {
            format!("
    #[serde(rename = \"{name}\")]
    {vis} {name}_: {type_},",
                name = name,
                type_ = type_::to_rust(type_),
                vis = visibility,
            )
        } else {

            format!("
    {vis} {name}: {type_},",
                name = name,
                type_ = type_::to_rust(type_),
                vis = visibility,
            )
        }
    }).collect();

    format!("
#[derive(serde::Serialize, serde::Deserialize)]
#[allow(dead_code)]
{vis} struct {name} {{{fields}
}}

#[allow(dead_code)]
const {upper}_VERSION: &str = \"{name}@{version}\";
#[allow(dead_code)]
const {upper}_CONTENT_TYPE: &str = \"application/json; ttype={name}@{version}\";
",
        name = name,
        fields = fields.join(""),
        vis = visibility,
        upper = upper,
        version = version,
    )
}
