use crate::{
    elm::{elm_type::ElmType, template},
    error::Result,
    schema,
};
use askama::Template;
use inflector::Inflector;
use std::collections::HashMap;

pub(crate) struct Struct<'v, 'n, 'f> {
    pub(crate) version: &'v str,
    pub(crate) name: &'n str,
    pub(crate) fields: &'f HashMap<String, schema::Field>,
}

impl<'v, 'n, 'f> Struct<'v, 'n, 'f> {
    pub(crate) fn type_info(&self) -> Result<template::TypeInfo> {
        let cap = self.name.to_pascal_case();
        let camel = self.name.to_camel_case();

        let fields: Vec<template::Field> = self
            .fields
            .iter()
            .map(|(name, type_)| template::Field {
                camel: name.to_camel_case(),
                elmtype: type_.to_elm(),
            })
            .collect();

        let core = template::Struct {
            name: &self.name,
            cap: &cap,
            camel: &camel,
            version: &self.version,
            fields: &fields,
        }
        .render()?;

        Ok(template::TypeInfo {
            core,
            encoder: "".to_string(),
            decoder: "".to_string(),
        })
    }
}
