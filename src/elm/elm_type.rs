use crate::schema::Type;

pub(crate) trait ElmType {
    fn to_elm(&self) -> String;
}

impl ElmType for Type {
    fn to_elm(&self) -> String {
        match self {
            Type::String => "String".to_string(),
            Type::Int => "Int".to_string(),
            Type::Float => "Float".to_string(),
            Type::Bool => "Bool".to_string(),
            Type::List(inner) => format!("List {}", inner.to_elm()),
            Type::Dict(inner) => format!("Dict {}", inner.to_elm()),
            Type::Optional(inner) => format!("Maybe {}", inner.to_elm()),
            Type::Alias(name, _) => name.clone(),
            Type::Struct(name, _) => name.clone(),
            Type::Union(name, _) => name.clone(),
        }
    }
}
