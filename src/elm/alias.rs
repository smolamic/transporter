use crate::{
    elm::{elm_type::ElmType, template},
    error::Result,
    schema,
};
use askama::Template;
use inflector::Inflector;

pub(crate) struct Alias<'v, 'n, 't> {
    pub(crate) version: &'v str,
    pub(crate) name: &'n str,
    pub(crate) type_: &'t schema::Type,
}

impl<'v, 'n, 't> Alias<'v, 'n, 't> {
    pub(crate) fn type_info(&self) -> Result<template::TypeInfo> {
        let cap = self.name.to_pascal_case();
        let camel = self.name.to_camel_case();
        let elmtype = self.type_.to_elm();

        let core = template::Alias {
            name: &self.name,
            cap: &cap,
            camel: &camel,
            version: &self.version,
            elmtype: &elmtype,
        }
        .render()?;

        Ok(template::TypeInfo {
            core,
            encoder: "".to_string(),
            decoder: "".to_string(),
        })
    }
}
