use crate::{
    code_generator::CodeGenerator,
    elm::{alias::Alias, struct_::Struct, template, union::Union},
    error::Result,
    file_creator::FileCreator,
    schema,
    util::ResultIterator,
};

use std::{io::Write, path::PathBuf};

use askama::Template;
use structopt::StructOpt;
use typed_builder::TypedBuilder;

#[derive(StructOpt, TypedBuilder)]
pub struct Cli {
    #[structopt(parse(from_os_str))]
    #[builder(setter(into))]
    output: PathBuf,
    #[structopt(short = "m", long = "module")]
    #[builder(setter(into))]
    module: String,
    #[structopt(
        short = "c",
        long = "config",
        parse(from_os_str),
        default_value = "Schema.toml"
    )]
    #[builder(default = "Schema.toml".into(), setter(into))]
    config: PathBuf,
}

impl CodeGenerator for Cli {
    fn build<C>(&self, fc: &mut C) -> Result<()>
    where
        C: FileCreator,
    {
        let schema = schema::Schema::try_from_file(&self.config)?;

        let type_info: Vec<template::TypeInfo> = schema
            .types()
            .filter_map(|type_| match type_.as_ref() {
                schema::Type::Struct(name, fields) => Some(
                    Struct {
                        version: &schema.version(),
                        name,
                        fields,
                    }
                    .type_info(),
                ),

                schema::Type::Union(name, variants) => Some(
                    Union {
                        version: &schema.version(),
                        name,
                        variants,
                    }
                    .type_info(),
                ),

                schema::Type::Alias(name, type_) => Some(
                    Alias {
                        version: &schema.version(),
                        name,
                        type_,
                    }
                    .type_info(),
                ),

                _ => None,
            })
            .vec_result()?;

        let mut core_path = self.output.clone();
        core_path.push(format!("{}.elm", self.module));
        let mut core = fc.create(&core_path)?;

        core.write_all(
            &template::Core {
                module: &self.module,
                types: &type_info,
            }
            .render()?
            .into_bytes(),
        )?;

        Ok(())
    }
}
