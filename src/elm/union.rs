use crate::{
    elm::{elm_type::ElmType, template},
    error::Result,
    schema,
};
use askama::Template;
use inflector::Inflector;
use std::collections::HashMap;

pub(crate) struct Union<'v, 'n, 'va> {
    pub(crate) version: &'v str,
    pub(crate) name: &'n str,
    pub(crate) variants: &'va HashMap<String, schema::Variant>,
}

impl<'v, 'n, 'va> Union<'v, 'n, 'va> {
    pub(crate) fn type_info(&self) -> Result<template::TypeInfo> {
        let cap = self.name.to_pascal_case();
        let camel = self.name.to_camel_case();

        let variants: Vec<template::Variant> = self
            .variants
            .iter()
            .map(|(name, fields)| {
                let cap = name.to_pascal_case();
                let fields: Vec<template::Field> = fields
                    .iter()
                    .map(|(name, type_)| template::Field {
                        camel: name.to_camel_case(),
                        elmtype: type_.to_elm(),
                    })
                    .collect();
                template::Variant {
                    cap: cap,
                    fields: fields,
                }
            })
            .collect();

        let core = template::Union {
            name: &self.name,
            cap: &cap,
            camel: &camel,
            variants: &variants,
            version: &self.version,
        }
        .render()?;

        Ok(template::TypeInfo {
            core,
            encoder: "".to_string(),
            decoder: "".to_string(),
        })
    }
}
