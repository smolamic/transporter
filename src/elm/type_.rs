use crate::error::Result;

pub(crate) trait Type {
    fn definition(&self) -> Result<String>;
    fn encoder(&self) -> Result<String>;
    fn decoder(&self) -> Result<String>;
}
