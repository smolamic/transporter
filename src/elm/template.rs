use askama::Template;

pub(crate) struct Field {
    pub(crate) camel: String,
    pub(crate) elmtype: String,
}

#[derive(Template)]
#[template(path = "elm/Struct.elm")]
pub(crate) struct Struct<'n, 'cap, 'cam, 'v, 'f> {
    pub(crate) name: &'n str,
    pub(crate) cap: &'cap str,
    pub(crate) camel: &'cam str,
    pub(crate) version: &'v str,
    pub(crate) fields: &'f Vec<Field>,
}

pub(crate) struct Variant {
    pub(crate) cap: String,
    pub(crate) fields: Vec<Field>,
}

#[derive(Template)]
#[template(path = "elm/Union.elm")]
pub(crate) struct Union<'n, 'cap, 'cam, 'v> {
    pub(crate) name: &'n str,
    pub(crate) cap: &'cap str,
    pub(crate) camel: &'cam str,
    pub(crate) version: &'v str,
    pub(crate) variants: &'v Vec<Variant>,
}

#[derive(Template)]
#[template(path = "elm/Alias.elm")]
pub(crate) struct Alias<'n, 'cap, 'cam, 'v, 't> {
    pub(crate) name: &'n str,
    pub(crate) cap: &'cap str,
    pub(crate) camel: &'cam str,
    pub(crate) version: &'v str,
    pub(crate) elmtype: &'t str,
}

pub(crate) struct TypeInfo {
    pub(crate) encoder: String,
    pub(crate) decoder: String,
    pub(crate) core: String,
}

#[derive(Template)]
#[template(path = "elm/Core.elm")]
pub(crate) struct Core<'m, 't> {
    pub(crate) module: &'m str,
    pub(crate) types: &'t Vec<TypeInfo>,
}
