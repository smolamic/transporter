use std::fmt;

const KEYWORDS: &'static [&'static str] = &["type"];

pub(crate) struct Escaper;

impl askama_escape::Escaper for Escaper {
    fn write_escaped<W>(&self, mut fmt: W, string: &str) -> fmt::Result
    where
        W: fmt::Write,
    {
        let mut string = string.to_string();
        for keyword in KEYWORDS.iter() {
            string = string.replace(keyword, &format!("{}_", keyword));
        }
        fmt.write_str(&string)
    }
}
