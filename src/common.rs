use std::collections::HashMap;

pub(crate) type Outfiles = HashMap<String, String>;
