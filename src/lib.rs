/*!
Transporter generates code for safely serializing and deserializing JSON data
in different languages.

Options are usually specified via command line.
*/
pub(crate) mod cli;
pub(crate) mod code_generator;
pub mod elm;
pub(crate) mod error;
pub(crate) mod file_creator;
pub mod php;
pub mod rust;
pub(crate) mod schema;
pub(crate) mod util;

pub use cli::Cli;
pub use code_generator::CodeGenerator;
pub use error::Result;
pub use file_creator::{FileCreator, FsFileCreator};
