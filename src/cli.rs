use crate::{code_generator::CodeGenerator, elm, error::Result, php, rust, FileCreator};
use structopt::StructOpt;

/*
!!!This appears in the binary's help (and cannot be overridden) if set as doc-comment!!!
Holds the main configuration for Transporter and builds generated code
accordingly using [CodeGenerator::build](trait.CodeGenerator.html#tymethod.build).

Use `Cli::from_args()` to parse command line arguments. Then use
[CodeGenerator::build](trait.CodeGenerator.html#tymethod.build)
to build the code for the language specified by the subcommand.

```
use transporter::Cli;
use structopt::StructOpt;

let cli = Cli::from_iter(vec!["transporter", "rust", "src/Api"]);

match cli {
    Cli::Rust(_) => println!("Running the \"rust\"-subcommand"),
    _ => unreachable!(),
}
```
*/
#[derive(StructOpt)]
#[structopt(name = "Transporter", about)]
pub enum Cli {
    Rust(rust::Cli),
    Php(php::Cli),
    Elm(elm::Cli),
}

impl CodeGenerator for Cli {
    fn build<C>(&self, fc: &mut C) -> Result<()>
    where
        C: FileCreator,
    {
        match self {
            Self::Rust(cli) => cli.build(fc),
            Self::Php(cli) => cli.build(fc),
            Self::Elm(cli) => cli.build(fc),
        }
    }
}
