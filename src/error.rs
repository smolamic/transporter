use std::{error, fmt, io, path::PathBuf, result};
use toml::de;

#[derive(Debug)]
pub enum Error {
    UnmatchedDelimiter(&'static str, String),
    UnresolvedType(String),
    DuplicateType(String),
    IOError(io::Error),
    ParseError(de::Error),
    InvalidPath(PathBuf),
    IncompatibleVersion(String),
    SemVerError(semver::SemVerError),
    ReqParseError(semver::ReqParseError),
    TemplateError(askama::Error),
    EmptyUnion(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Self::UnmatchedDelimiter(delimiter, type_) => write!(
                f,
                "Unmatched delimiter \"{}\" in type \"{}\"",
                delimiter, type_
            ),

            Self::UnresolvedType(type_) => write!(f, "Unresolved type \"{}\"", type_),

            Self::DuplicateType(type_) => write!(f, "Duplicate type definition for \"{}\"", type_),

            Self::IOError(inner) => write!(f, "{}", inner),

            Self::ParseError(inner) => write!(f, "{}", inner),

            Self::InvalidPath(path) => write!(f, "Invalid Path: {}", path.display()),

            Self::IncompatibleVersion(version) => write!(
                f,
                "The specified schema was written for the incompatible transporter version {}",
                version
            ),

            Self::SemVerError(inner) => write!(f, "{}", inner),

            Self::ReqParseError(inner) => write!(f, "{}", inner),

            Self::TemplateError(inner) => write!(f, "{}", inner),

            Self::EmptyUnion(name) => write!(f, "Union \"{}\" needs at least one Variant", name,),
        }
    }
}

impl error::Error for Error {}

impl From<io::Error> for Error {
    fn from(inner: io::Error) -> Self {
        Self::IOError(inner)
    }
}

impl From<de::Error> for Error {
    fn from(inner: de::Error) -> Self {
        Self::ParseError(inner)
    }
}

impl From<semver::SemVerError> for Error {
    fn from(inner: semver::SemVerError) -> Self {
        Self::SemVerError(inner)
    }
}

impl From<semver::ReqParseError> for Error {
    fn from(inner: semver::ReqParseError) -> Self {
        Self::ReqParseError(inner)
    }
}

impl From<askama::Error> for Error {
    fn from(inner: askama::Error) -> Self {
        Self::TemplateError(inner)
    }
}

pub type Result<T> = result::Result<T, Error>;
