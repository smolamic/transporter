pub(crate) trait Indent {
    fn indent(&self, space: usize) -> String;
}

impl Indent for str {
    fn indent(&self, spaces: usize) -> String {
        self.lines()
            .map(|line| format!("{:indent$}{}", "", line, indent = spaces))
            .collect::<Vec<String>>()
            .join("\n")
    }
}

impl Indent for String {
    fn indent(&self, spaces: usize) -> String {
        self.lines()
            .map(|line| format!("{:indent$}{}", "", line, indent = spaces))
            .collect::<Vec<String>>()
            .join("\n")
    }
}

pub(crate) trait ResultIterator {
    type Item;
    type Error;
    fn vec_result(self) -> Result<Vec<Self::Item>, Self::Error>;
}

impl<I, R, E> ResultIterator for I
where
    I: Iterator<Item = Result<R, E>>,
{
    type Item = R;
    type Error = E;
    fn vec_result(self) -> Result<Vec<Self::Item>, Self::Error> {
        let min_size = self.size_hint().0;

        self.fold(
            Ok(Vec::with_capacity(min_size)),
            |collection, item| match collection {
                Ok(mut collection) => match item {
                    Ok(item) => {
                        collection.push(item);
                        Ok(collection)
                    }
                    Err(error) => Err(error),
                },
                Err(error) => Err(error),
            },
        )
    }
}
