use crate::{error::Result, FileCreator};

/**
Represents a structure from which one or more source-code files can be built

A CodeGenerator can build some code that is written to one or multiple
source-code files using a [FileCreator](trait.FileCreator.html).

```
use transporter::{
    CodeGenerator,
    Result,
    FileCreator,
};
use std::{
    io::Write,
    path::Path,
};

struct UnionStruct {
    name: String,
}

impl CodeGenerator for UnionStruct {
    fn build<C>(&self, fc: &mut C) -> Result<()> where
        C: FileCreator,
    {
        let mut file = fc.create(Path::new(&format!("{}.rs", self.name)).into())?;

        file.write_all(&(format!("struct {};", self.name).into_bytes()));

        Ok(())
    }
}

let null = UnionStruct { name: "Null".to_string() };
```
*/
pub trait CodeGenerator {
    fn build<C>(&self, fc: &mut C) -> Result<()>
    where
        C: FileCreator;
}
