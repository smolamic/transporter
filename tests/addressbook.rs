use std::{path::Path, process::Command};
use transporter::{elm, php, rust, CodeGenerator, FsFileCreator};

#[test]
#[ignore]
fn rust() {
    let outfile = Path::new("e2e/rust/src/lib.rs");

    let cli = rust::Cli::builder()
        .output(outfile)
        .config("schema/Addressbook.toml")
        .visibility("pub")
        .build();

    let mut fc = FsFileCreator::new();

    cli.build(&mut fc).unwrap();

    assert!(outfile.is_file());

    assert!(Command::new("sh")
        .args(&["-c", "cd e2e/rust && cargo test"])
        .status()
        .expect("Failed to execute cargo test")
        .success());
}

#[test]
#[ignore]
fn php() {
    let outdir = Path::new("e2e/php/src/Addressbook");
    let cli = php::Cli::builder()
        .output(&outdir)
        .config("schema/Addressbook.toml")
        .namespace("TransporterTest\\Addressbook")
        .build();

    let mut fc = FsFileCreator::new();
    cli.build(&mut fc).unwrap();

    assert!(outdir.is_dir());

    let files = [
        "Addressbook.php",
        "PhoneNumber.php",
        "PhoneNumberType.php",
        "Contact.php",
        "TransportType.php",
        "PhoneNumberType/Custom.php",
        "PhoneNumberType/Home.php",
        "PhoneNumberType/Mobile.php",
        "PhoneNumberType/Work.php",
    ];

    for file_name in &files {
        let path = outdir.join(file_name);

        assert!(
            path.is_file(),
            format!("outfile \"{}\" has been created", file_name)
        );
    }

    assert!(
        Command::new("sh")
            .args(&[
                "-c",
                "cd e2e/php/ && composer dump-autoload && cd tests && phpunit"
            ])
            .status()
            .expect("Failed to execute phpunit test")
            .success(),
        "tests succeeded"
    );
}

#[test]
#[ignore]
fn elm() {
    println!("Test ELM");
    let outdir = Path::new("e2e/elm/src");
    let cli = elm::Cli::builder()
        .output(&outdir)
        .config("schema/Addressbook.toml")
        .module("Addressbook")
        .build();

    let mut fc = FsFileCreator::new();
    cli.build(&mut fc).unwrap();

    assert!(outdir.is_dir());

    let files = ["Addressbook.elm"];

    for file_name in &files {
        println!("Checking file {}", file_name);
        let path = outdir.join(file_name);

        assert!(
            path.is_file(),
            format!("outfile \"{}\" has been created", file_name)
        );
    }

    assert!(
        Command::new("sh")
            .args(&["-c", "cd e2e/elm/ && elm-test"])
            .status()
            .expect("Failed to execute elm test")
            .success(),
        "tests succeeded"
    );
}
